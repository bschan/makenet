﻿using MakeNet.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MakeNet
{
    class Job
    {
        public Processor Processor { get; }
        public Target Target { get; }
        public string CallerDependency { get; }
        public bool AllowAsync { set;  get; }

        public Job(Processor processor, Target target, string callerDependency)
        {
            this.AllowAsync = true;
            this.Processor = processor;
            this.Target = target;
            this.CallerDependency = callerDependency;
        }
    }
}
