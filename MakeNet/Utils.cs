﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace MakeNet
{
    public class Utils
    {
        // Holds application name as key and complete path as value
        private static Dictionary<string, string> pathApplicationsCache = new Dictionary<string, string>();

        public static void InitializePathApplicationCache()
        {
            string envTotalPath = Environment.GetEnvironmentVariable("PATH");
            string[] envPaths = envTotalPath.Split(new char[] { ';' });
            foreach(string envPath in envPaths)
            {
                if (!Directory.Exists(envPath)) continue;

                string[] files = Directory.GetFiles(envPath);
                foreach(string file in files)
                {
                    if (file.EndsWith(".bat") || file.EndsWith(".exe") || file.EndsWith(".vbs"))
                    {
                        string appName = Path.GetFileNameWithoutExtension(file);
                        if (!pathApplicationsCache.ContainsKey(appName))
                        {                         
                            pathApplicationsCache.Add(appName, file);
                        }
                    }
                }
            }
            Debug.WriteLine("Found " + pathApplicationsCache.Count + " applications in environment Paths");
        }

        public static bool ExecuteCmdProcess(string workingDir, string command, string arguments, bool directOutput, out List<string> output)
        {
            string directPath = null;

            // Normalize slashes
            //------------------
            command = command.Replace("/", "\\");

            // Check if command contains path to application
            //----------------------------------------------
            if(command.Contains("\\"))
            {
                directPath = command;
            }

            // Remove file extension from command
            //-----------------------------------
            if (command.EndsWith(".exe"))
            {
                command = command.Substring(0, command.Length - ".exe".Length);
            }

            // Check if cached application matches requested command
            //------------------------------------------------------
            if (directPath != null || pathApplicationsCache.TryGetValue(command, out directPath))
            {
                return ExecuteProcess(workingDir, directPath, arguments, directOutput, out output);
            }
            // Else execute via CMD
            //---------------------
            else
            {
                return ExecuteProcess(workingDir, "cmd.exe", "/c \"" + command + " " + arguments + " 2>&1\"", directOutput, out output);
            }
        }

        public static bool ExecuteProcess(string workingDir, string command, string arguments, bool directoutput, out List<string> output)
        {
            output = new List<string>();

            try
            {
                using (Process proc = new Process())
                {
                    proc.StartInfo.FileName = command;
                    proc.StartInfo.Arguments = arguments;
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.StartInfo.WorkingDirectory = workingDir;

                    proc.Start();
                    while (!proc.StandardOutput.EndOfStream)
                    {
                        string line = proc.StandardOutput.ReadLine();
                        if(directoutput)
                        {
                            Console.WriteLine(line);
                        }
                        output.Add(line);
                    }
                    proc.WaitForExit();
                    int exitCode = proc.ExitCode;

                    if (exitCode != 0)
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public static string GetFormattedTimeSpan(TimeSpan timeSpan)
        {
            string formattedTimeSpan = "";

            if (timeSpan.Days != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Days + "d";
            }

            if (timeSpan.Hours != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Hours + "h";
            }

            if (timeSpan.Minutes != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Minutes + "m";
            }

            //if (timeSpan.Seconds != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Seconds + "." + timeSpan.Milliseconds + "s";
            }

            /*
            if (timeSpan.TotalSeconds == 0 && timeSpan.Milliseconds != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Milliseconds + "ms";
            }
            */

            return formattedTimeSpan;
        }

        public static string GetNormalizedPath(string rawPath)
        {
            // Check for Cygwin paths ('/mnt/c/...')
            //-----------------------
            Regex regex = new Regex(@"^(([\\|\/]?mnt)?[\\|\/]?([a-zA-Z]+)[\\|\/]+)(.*)");
            Match match = regex.Match(rawPath);
            if (match.Success)
            {
                string cygwinDrivePart = match.Groups[1].Value;
                string driveLetter = match.Groups[4].Value;

                rawPath = driveLetter + ":/" + rawPath.Substring(cygwinDrivePart.Length);
            }

            return rawPath;
        }

        public static string GetRelativePath(string currentPath, string totalPath)
        {
            if (IsSubDirectoryOf(totalPath, currentPath))
            {
                return ".\\" + MakeRelativePath(currentPath, totalPath);
            }
            return totalPath;
        }

        public static bool IsSubDirectoryOf(string candidate, string other)
        {
            var isChild = false;
            try
            {
                var candidateInfo = new DirectoryInfo(candidate);
                var otherInfo = new DirectoryInfo(other);

                while (candidateInfo.Parent != null)
                {
                    if (candidateInfo.Parent.FullName == otherInfo.FullName)
                    {
                        isChild = true;
                        break;
                    }
                    else candidateInfo = candidateInfo.Parent;
                }
            }
            catch (Exception error)
            {
                var message = String.Format("Unable to check directories {0} and {1}: {2}", candidate, other, error);
                Console.WriteLine(message);
            }

            return isChild;
        }

        /// <summary>
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path or <c>toPath</c> if the paths are not related.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static String MakeRelativePath(String fromPath, String toPath)
        {
            if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
            if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

            if(!fromPath.EndsWith("\\"))
            {
                fromPath += "\\";
            }

            Uri fromUri = new Uri(fromPath);
            Uri toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            String relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
            {
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            }

            return relativePath;
        }

        public static bool HasWriteAccessToFolder(string folderPath)
        {
            try
            {
                // Attempt to get a list of security permissions from the folder. 
                // This will raise an exception if the path is read only or do not have access to view the permissions. 
                System.Security.AccessControl.DirectorySecurity ds = Directory.GetAccessControl(folderPath);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
        }

        public static void AppendToEnvironmentVariable(string variableName, string append)
        {
            Debug.WriteLine("Append to environment variable '" + variableName + "': " + append);

            string envPath = Environment.GetEnvironmentVariable(variableName);
            string newPath = envPath + ";" + append;
            Environment.SetEnvironmentVariable(variableName, newPath);
        }
    }
}
