﻿using MakeNet.Parser;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace MakeNet
{
    class Processor
    {
        private Parser.Parser makefileParser;
        private Makefile parsedMakefile;
        private int asyncThreadsMaxNum;
        private int asyncFailed = 0;
        private int asyncStarted = 0;
        private int asyncFinished = 0;
        private DateTime startProcessingTime;

        public Processor(Parser.Parser makefileParser, Makefile parsedMakefile, int jobsNum)
        {
            this.makefileParser = makefileParser;
            this.parsedMakefile = parsedMakefile;
            this.asyncThreadsMaxNum = (jobsNum > 0) ? jobsNum : 1;
        }

        public bool ExecuteTargets(List<string> targets)
        {
            bool retval = true;

            // Reset async processing
            //-----------------------
            asyncFailed = 0;
            asyncStarted = 0;
            asyncFinished = 0;
            ThreadPool.SetMinThreads(asyncThreadsMaxNum, 0);
            ThreadPool.SetMaxThreads(asyncThreadsMaxNum, 0);

            // Set start time
            //---------------
            startProcessingTime = DateTime.Now;

            foreach (string targetName in targets)
            {
                Target target = parsedMakefile.GetTarget(targetName);
                if(target == null)
                {
                    Console.WriteLine("Could not find target '" + targetName + "'");
                    return false;
                }

                // Execute found target
                //---------------------
                Job job = new Job(this, target, null);
                if(!Execute(job))
                {
                    Console.WriteLine("Failed to execute target '" + targetName + "'");
                    retval = false;
                }
            }

            return retval;
        }

        private bool Execute(Job job)
        {
            string fullTargetName = job.Target.Name;
            string dynamicTargetname = job.Target.Name;
            string firstDependencyName = "";

            if(job.Target.Name.StartsWith("%") && job.CallerDependency != null)
            {
                int replacementLength = job.Target.Name.Length - 1; // e.g. for '%.o' = 2
                if(job.CallerDependency.Length < replacementLength)
                {
                    Console.WriteLine("Wrong caller dependency '" + job.CallerDependency + "' for target '" + job.Target.Name + "'");
                    return false;
                }
                dynamicTargetname = job.CallerDependency.Substring(0, job.CallerDependency.Length - replacementLength);
                fullTargetName = dynamicTargetname + job.Target.Name.Substring(1);
            }
            else
            {
                Debug.WriteLine("Execute target '" + job.Target.Name + "'");
            }


            // Handle dependencies
            //--------------------
            if(job.Target.Dependencies.Count > 0)
            {
                bool targetNotRequired = false;

                // Process each dependency
                //------------------------
                foreach(string dependency in job.Target.Dependencies)
                {
                    string fullDependencyName = dependency;
                    bool dependencyIsFile = false;

                    // Replace wildcard
                    //-----------------
                    if(fullDependencyName.StartsWith("%"))
                    {
                        fullDependencyName = dynamicTargetname + fullDependencyName.Substring(1);
                    }

                    if (firstDependencyName.Length == 0)
                    {
                        firstDependencyName = fullDependencyName;
                    }

                    // If not a phony we check if dependency is a file
                    //------------------------------------------------
                    string dependencyFilePath = Path.Combine(makefileParser.CurrentPath, fullDependencyName);
                    if (!parsedMakefile.IsPhony(fullDependencyName))
                    {
                        if(File.Exists(dependencyFilePath))
                        {
                            dependencyIsFile = true;

                            string targetFilePath = Path.Combine(makefileParser.CurrentPath, fullTargetName);

                            // Check if target file does not exsist
                            //-------------------------------------
                            if (File.Exists(targetFilePath))
                            {
                                // Check if target file is newer than dependecy 
                                // -> no update required
                                //---------------------------------------------
                                DateTime dependencyFileTime = File.GetLastWriteTime(dependencyFilePath);
                                DateTime targetFileTime = File.GetLastWriteTime(targetFilePath);

                                int compare = DateTime.Compare(dependencyFileTime, targetFileTime);
                                if (compare <= 0)
                                {
                                    targetNotRequired = true;
                                    continue;
                                }
                            }
                        }
                    }

                    targetNotRequired = false;

                    // Get matching dependency target
                    //-------------------------------
                    Target dependencyTarget = parsedMakefile.GetTarget(fullDependencyName);
                    if(dependencyTarget == null && !dependencyIsFile)
                    {
                        return false;
                    }

                    // Execute dependency target
                    //--------------------------
                    if (dependencyTarget != null)
                    {
                        Job dependencyJob = new Job(this, dependencyTarget, dependency);
                        if (job.AllowAsync && asyncThreadsMaxNum > 1)
                        {
                            ExecuteAsync(dependencyJob);
                        }
                        else
                        {
                            Execute(dependencyJob);
                        }
                    }
                }

                // Skip this target if not required
                // e.g. all dependencies are up to date
                //-------------------------------------
                if(targetNotRequired)
                {
                    return true;
                }

                // Wait for async jobs
                //--------------------
                if (job.AllowAsync)
                {
                    WaitForAsyncAllDone();
                }

                if(asyncFailed > 0)
                {
                    return false;
                }
            }

            // Start time
            //-----------
            DateTime relativeTime = DateTime.Now;

            // Process content
            //----------------
            foreach(string content in job.Target.Content)
            {
                bool silent = false;
                bool errorTolerant = false;
                string command = null;
                string parameter = "";
                string parsedContent = content;

                // Check for silence char '@'
                //---------------------------
                if(parsedContent.StartsWith("@"))
                {
                    silent = true;
                    parsedContent = parsedContent.Substring(1);
                }

                // Check for error tolerance
                //--------------------------
                if(parsedContent.StartsWith("-"))
                {
                    errorTolerant = true;
                    parsedContent = parsedContent.Substring(1);
                }

                // Replace automatic variables like $*, $<, $@
                //--------------------------------------------
                {
                    // $@ name of real target name 
                    // (e.g. '%.o: %.c' $@ will be name of %.o)
                    parsedContent = parsedContent.Replace("$@", fullTargetName);

                    // $< name of dependency 
                    // (e.g. '%.o: %.c' $< will be name of %.c)
                    parsedContent = parsedContent.Replace("$<", firstDependencyName);

                    // $* name of dynamic target name
                    // (e.g. '%.o: %.c' $* will be name of substituted %.)
                    parsedContent = parsedContent.Replace("$*", dynamicTargetname);

                    // $time_rel Time since start of target or last read of $time_rel
                    if(parsedContent.Contains("$~time_rel"))
                    {
                        TimeSpan timeDelta = DateTime.Now.Subtract(relativeTime);
                        string relativeTimeString;
                        relativeTimeString = Utils.GetFormattedTimeSpan(timeDelta);
                        parsedContent = parsedContent.Replace("$~time_rel", relativeTimeString);
                        relativeTime = DateTime.Now;
                    }

                    // $time_abs Time since start of processing of targets
                    if (parsedContent.Contains("$~time_abs"))
                    {
                        TimeSpan timeDelta = DateTime.Now.Subtract(startProcessingTime);
                        string relativeTimeString;
                        relativeTimeString = Utils.GetFormattedTimeSpan(timeDelta);
                        parsedContent = parsedContent.Replace("$~time_abs", relativeTimeString);
                    }
                }

                // Parse content line with normal makefile rules
                //----------------------------------------------
                if(!makefileParser.Commands.ProcessCommands(parsedContent, out parsedContent, null, 0))
                {
                    return false;
                }

                // Split for command and parameter
                //--------------------------------
                string[] contentParts = parsedContent.Split(new char[] { ' ' }, 2);
                if(contentParts.Length < 1)
                {
                    return false;
                }
                command = contentParts[0];

                if (contentParts.Length > 1)
                {
                    parameter = contentParts[1];
                }

                // Execute command
                //----------------
                if(!silent)
                {
                    Console.WriteLine(command + " " + parameter);
                }

                string result;
                bool success = makefileParser.Commands.ProcessCommand(command, parameter, !errorTolerant, out result);
                if(!success)
                {
                    if(command.Equals("__MAKE__"))
                    {
                        Console.WriteLine("Error while executing sub makefile");
                    }
                    else
                    {
                        Console.WriteLine("Error while executing command '" + command + "'");
                    }
                }

                if(!success && !errorTolerant)
                {
                    return false;
                }
            }

            return true;
        }
    
        private void ExecuteAsync(Job job)
        {
            // Check if any other job already failed
            //--------------------------------------
            if(asyncFailed > 0)
            {
                return;
            }

            asyncStarted++;
            ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessAsync), job);
        }

        private void WaitForAsyncAllDone()
        {
            // Wait until all jobs are done
            //-----------------------------
            while (asyncFinished < asyncStarted)
            {
                Thread.Sleep(100);
            }

            return;
        }

        public static void ProcessAsync(object obj)
        {
            Job job = (Job)obj;
            
            job.AllowAsync = false;

            // Only run if no other job already failed
            //----------------------------------------
            if (job.Processor.asyncFailed == 0)
            {
                bool success = job.Processor.Execute(job);
                if (!success)
                {
                    Interlocked.Increment(ref job.Processor.asyncFailed);
                }
            }

            Interlocked.Increment(ref job.Processor.asyncFinished);
        }
    }
}
